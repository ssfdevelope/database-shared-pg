FROM postgres:13.12

ENV POSTGRES_PASSWORD docker
ENV POSTGRES_USER docker
COPY create-databases.sql /docker-entrypoint-initdb.d/create-databases.sql
EXPOSE 5432
CMD ["postgres"]
