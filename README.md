Shared postgres for all SSF projects. Note that this is for local development.

# Run first time

We now advise everyone to use a named volume to keep data between updates. Updates are still possible by exec'ing, as described under 'Updates'

```sh
docker volume create database-shared-pg
docker run --name database-shared-pg -v database-shared-pg:/var/lib/postgresql/data -p 5432:5432 -d --restart always registry.gitlab.com/ssfdevelope/database-shared-pg
```

This pulls, starts and runs on every reboot a postgres docker container with every database we have.

# Updates

From time to time a new database has been added to _create-databases.sql_
This means a new image is pushed to registry.gitlab.com/ssfdevelope/database-shared-pg:latest and we need to update our own locally.

PS:
There have been various ways of running the docker container.
The update method described here will only work if you have done the guide under 'first time' above.
If you have not done that before, I suggest you wipe all the previous containers and volumes you have before doing the 'first time' described above.

```sh
docker container stop database-shared-pg
docker container rm database-shared-pg
docker pull registry.gitlab.com/ssfdevelope/database-shared-pg
docker run --name database-shared-pg -v database-shared-pg:/var/lib/postgresql/data -p 5432:5432 -d --restart always registry.gitlab.com/ssfdevelope/database-shared-pg
docker exec -it database-shared-pg psql -U docker -f /docker-entrypoint-initdb.d/create-databases.sql
```

# Wiping docker container and volumes

Check for either the container id or name:

```sh
docker container ls
```

Find all container id's of containers running registry.gitlab.com/ssfdevelope/database-shared-pg as image.

Then do the following. Swap $CONTAINER_ID with the container id you found using `ls`

```sh
docker container stop $CONTAINER_ID
docker container rm -v $CONTAINER_ID
```

If you have named volumes previously attached, find and remove those. Often an anonymous volume has been used, then you will not have named volumes and are done.

```sh
docker volume ls
```

```sh
docker volume rm <volume_name>
```
